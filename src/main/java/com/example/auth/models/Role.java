package com.example.auth.models;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
