package com.example.auth.controllers;

import com.example.auth.dto.JwtAuthenticationResponse;
import com.example.auth.dto.SignUpRequest;
import com.example.auth.dto.SignInRequest;
import com.example.auth.services.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/signup")
    public JwtAuthenticationResponse signUp(@RequestBody SignUpRequest request) {
        return authenticationService.signUp(request);
    }

    @PostMapping("/signin")
    public JwtAuthenticationResponse signIn(@RequestBody SignInRequest request) {
        return authenticationService.signIn(request);
    }

    @GetMapping("/validate")
    public ResponseEntity<Void> validate() {
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}