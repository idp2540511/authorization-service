package com.example.auth.services;

import com.example.auth.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class UserService {

    @Value("${docker.ioService}")
    private String ioServiceURL;

    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                ResponseEntity<User> response = findByEmail(username);
                if (!response.getStatusCode().equals(HttpStatus.OK)) {
                    throw new UsernameNotFoundException("User not found");
                }

                return response.getBody();
            }
        };
    }

    public User save(User newUser) {
        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<User> responseType =
                new ParameterizedTypeReference<>(){
                };
        ResponseEntity<User> response = restTemplate.exchange(ioServiceURL + "/api/users/add",
                HttpMethod.POST,
                new HttpEntity<>(newUser),
                responseType);

        return response.getBody();
    }

    public ResponseEntity<User> findByEmail(String email) {
        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<User> responseType =
                new ParameterizedTypeReference<User>() {
                };

        return restTemplate.exchange(ioServiceURL + "/api/users/find-user/" + email,
                HttpMethod.GET,
                null,
                responseType);
    }
}
