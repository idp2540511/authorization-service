FROM openjdk:17-alpine
WORKDIR /app
COPY target/auth-0.0.1-SNAPSHOT.jar /app/auth.jar
EXPOSE 8282